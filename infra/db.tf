# ---------------------------------------------- #
#               Deploy DBaaS MySQL               #
# ---------------------------------------------- #

# Create managed MySQL DB Master instance
resource "vkcs_db_instance" "db01" {
  name              = "db01"
  keypair           = var.keypair_name
  flavor_id         = data.vkcs_compute_flavor.dbaas.id
  availability_zone = "GZ1"
  size              = 8
  volume_type       = "ceph-ssd"

  datastore {
    type    = "mysql"
    version = "8.0"
  }

  disk_autoexpand {
    autoexpand    = true
    max_disk_size = 1000
  }

  network {
    uuid = vkcs_networking_network.project_network.id
  }

  capabilities {
    name = "node_exporter"
    settings = {
      "listen_port" : "9100"
    }
  }

  capabilities {
    name = "mysqld_exporter"
    settings = {
      "listen_port" : "9104"
    }
  }
  
  depends_on = [
    vkcs_networking_subnet.project_subnet,
    vkcs_networking_router_interface.router_interface
  ]
}

# Create MySQL database
resource "vkcs_db_database" "wp-database" {
  name    = "wpdb"
  dbms_id = vkcs_db_instance.db01.id
  charset = "utf8"
  collate = "utf8_general_ci"
}

# Create MySQL user and password
resource "vkcs_db_user" "db-user" {
  name      = var.wp_username
  password  = var.wp_password
  dbms_id   = vkcs_db_instance.db01.id
  databases = [vkcs_db_database.wp-database.name]
}
