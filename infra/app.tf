# ---------------------------------------------- #
#           Deploy Wordpress app VMs             #
# ---------------------------------------------- #

# Create APP instance APP01
resource "vkcs_compute_instance" "app01" {
  name      = var.app01_name
  flavor_id = data.vkcs_compute_flavor.app.id
  security_groups = [
    vkcs_networking_secgroup.app.name
  ]
  image_name        = data.vkcs_images_image.ubuntu.name
  availability_zone = "GZ1"
  key_pair          = var.keypair_name
  config_drive      = true
  user_data = templatefile("../product/app-init.sh", {
    NFS_ADDRESS  = vkcs_compute_instance.nfs01.access_ip_v4,
    DB01_ADDRESS = vkcs_db_instance.db01.ip[0],
    WP_DATABASE  = var.wp_database,
    WP_USER      = var.wp_username,
    WP_PASSWORD  = var.wp_password,
    NODE_VERSION = var.node_exporter_version,
    NGINX_VERSION = var.nginx_exporter_version
  })

  block_device {
    uuid                  = data.vkcs_images_image.ubuntu.id
    source_type           = "image"
    destination_type      = "volume"
    volume_type           = var.app_volume_type
    volume_size           = var.app_volume_size
    boot_index            = 0
    delete_on_termination = true
  }

  network {
    uuid        = vkcs_networking_network.project_network.id
    fixed_ip_v4 = "10.0.1.100"
  }
  depends_on = [
    vkcs_networking_router_interface.router_interface,
    vkcs_db_instance.db01,
    vkcs_compute_instance.nfs01
  ]
}

# Create APP instance APP02
resource "vkcs_compute_instance" "app02" {
  name      = var.app02_name
  flavor_id = data.vkcs_compute_flavor.app.id
  security_groups = [
    vkcs_networking_secgroup.app.name
  ]
  image_name        = data.vkcs_images_image.ubuntu.name
  availability_zone = "MS1"
  key_pair          = var.keypair_name
  config_drive      = true
  user_data = templatefile("../product/app-init.sh", {
    NFS_ADDRESS  = vkcs_compute_instance.nfs01.access_ip_v4,
    DB01_ADDRESS = vkcs_db_instance.db01.ip[0],
    WP_DATABASE  = var.wp_database,
    WP_USER      = var.wp_username,
    WP_PASSWORD  = var.wp_password,
    NODE_VERSION = var.node_exporter_version,
    NGINX_VERSION = var.nginx_exporter_version
  })

  block_device {
    uuid                  = data.vkcs_images_image.ubuntu.id
    source_type           = "image"
    destination_type      = "volume"
    volume_type           = var.app_volume_type
    volume_size           = var.app_volume_size
    boot_index            = 0
    delete_on_termination = true
  }

  network {
    uuid        = vkcs_networking_network.project_network.id
    fixed_ip_v4 = "10.0.1.101"
  }
  depends_on = [
    vkcs_networking_router_interface.router_interface,
    vkcs_db_instance.db01,
    vkcs_compute_instance.nfs01
  ]
}
