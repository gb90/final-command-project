#!/bin/bash

# Update the package manager
sudo apt update

# ---------------------------------------------- #
#                Install Wordpress               #
# ---------------------------------------------- #

# Install Nginx
sudo apt install nginx -y

# Start and enable Nginx
sudo systemctl start nginx
sudo systemctl enable nginx

# Install MySQL client
sudo apt install mysql-client -y

# Install PHP and required extensions
sudo apt install php-fpm php-mysql php-curl php-gd php-mbstring php-xml php-xmlrpc -y

# Configure NFS mount
sudo apt install nfs-common -y
sudo mkdir -p /var/www/html/wp-content
sudo chown www-data:www-data /var/www/html/wp-content
sudo echo "${NFS_ADDRESS}:/wordpress /var/www/html/wp-content nfs rw,sync,hard,intr 0 0" >> /etc/fstab
sudo mount -a

# Configure PHP for Nginx
echo -e '<IfModule mod_setenvif.c>\n\tSetEnvIf X-Forwarded-Proto "^https$" HTTPS\n</IfModule>' > /etc/httpd/conf.d/xforwarded.conf
sed -i 's/post_max_size = 8M/post_max_size = 128M/g'  /etc/php/7.4/fpm/php.ini
sed -i 's/upload_max_filesize = 2M/upload_max_filesize = 128M/g'  /etc/php/7.4/fpm/php.ini
sed -i 's/max_execution_time = 30/max_execution_time = 600/g'  /etc/php/7.4/fpm/php.ini
sed -i 's/; max_input_vars = 1000/max_input_vars = 2000/g'  /etc/php/7.4/fpm/php.ini
sed -i 's/max_input_time = 60/max_input_time = 300/g'  /etc/php/7.4/fpm/php.ini
sudo systemctl restart php7.4-fpm

# Download and extract WordPress
cd /tmp
wget https://wordpress.org/latest.tar.gz
tar -xvzf latest.tar.gz
rm latest.tar.gz

# Copy WordPress files to Nginx web root
sudo cp -R /tmp/wordpress/* /var/www/html/
sudo rm -rf /tmp/wordpress

# Set permissions on WordPress files
sudo chown -R www-data:www-data /var/www/html/
sudo chmod -R 755 /var/www/html/

# Create WordPress configuration file
sudo mv /var/www/html/wp-config-sample.php /var/www/html/wp-config.php

# Configure Wordpress
sudo sed -i "s/database_name_here/${WP_DATABASE}/" /var/www/html/wp-config.php
sudo sed -i "s/username_here/${WP_USER}/" /var/www/html/wp-config.php
sudo sed -i "s/password_here/${WP_PASSWORD}/" /var/www/html/wp-config.php
sudo sed -i "s/localhost/${DB01_ADDRESS}/" /var/www/html/wp-config.php
sudo rm /var/www/html/index.html

# Configure nginx
sudo echo '
server {
    listen 80;
    listen [::]:80;
    listen 443;
    listen [::]:443;
	listen localhost:81;
	location /metrics {
		stub_status on;
	}
    root /var/www/html;
    index  index.php index.html index.htm;
    server_name  wordpress;

    client_max_body_size 100M;
    autoindex off;
    location / {
        try_files $uri $uri/ /index.php?$args;
    }

    location ~ \.php$ {
         include snippets/fastcgi-php.conf;
         fastcgi_pass unix:/var/run/php/php-fpm.sock;
         fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
         include fastcgi_params;
    }
}
' > /tmp/default
sudo mv /tmp/default /etc/nginx/sites-available/default
sudo chmod -R 755 /etc/nginx/sites-available/default

# Restart Nginx and PHP
sudo systemctl restart nginx
sudo systemctl restart php7.4-fpm

# ---------------------------------------------- #
#             Install NodeExporter               #
# ---------------------------------------------- #

# Make node_exporter user
sudo adduser --no-create-home --disabled-login --shell /bin/false --gecos "Node Exporter User" node_exporter

# Download node_exporter and copy utilities to where they should be in the filesystem
wget https://github.com/prometheus/node_exporter/releases/download/v${NODE_VERSION}/node_exporter-${NODE_VERSION}.linux-amd64.tar.gz
tar xvzf node_exporter-${NODE_VERSION}.linux-amd64.tar.gz

sudo cp node_exporter-${NODE_VERSION}.linux-amd64/node_exporter /usr/local/bin/
sudo chown node_exporter:node_exporter /usr/local/bin/node_exporter

# systemd
sudo echo '
[Unit]
Description=Node Exporter
Wants=network-online.target
After=network-online.target

[Service]
User=node_exporter
Group=node_exporter
Type=simple
ExecStart=/usr/local/bin/node_exporter

[Install]
WantedBy=multi-user.target
' > /etc/systemd/system/node_exporter.service

sudo systemctl daemon-reload
sudo systemctl enable node_exporter
sudo systemctl start node_exporter

# Installation cleanup
rm node_exporter-${NODE_VERSION}.linux-amd64.tar.gz
rm -rf node_exporter-${NODE_VERSION}.linux-amd64

# ---------------------------------------------- #
#             Install NginXExporter              #
# ---------------------------------------------- #

# Change directories
cd /tmp

# Download NginX Exporter
wget https://github.com/nginxinc/nginx-prometheus-exporter/releases/download/v${NGINX_VERSION}/nginx-prometheus-exporter_${NGINX_VERSION}_linux_amd64.tar.gz
tar -zxf nginx-prometheus-exporter_${NGINX_VERSION}_linux_amd64.tar.gz

# Create user
sudo adduser --no-create-home --disabled-login --shell /bin/false --gecos "NginX Exporter User" nginx_exporter

# Update NginX Exporter ownership
sudo cp nginx-prometheus-exporter /usr/local/bin/
sudo chown nginx_exporter:nginx_exporter /usr/local/bin/nginx-prometheus-exporter

# Populate NginX Exporter configuration file
sudo echo '
[Unit]
Description=NGINX Prometheus Exporter
After=network.target

[Service]
Type=simple
User=nginx_exporter
Group=nginx_exporter
ExecStart=/usr/local/bin/nginx-prometheus-exporter \
    -web.listen-address=0.0.0.0:9113 \
    -nginx.scrape-uri http://127.0.0.1:81/metrics

SyslogIdentifier=nginx_prometheus_exporter
Restart=always

[Install]
WantedBy=multi-user.target
' > /etc/systemd/system/nginx-exporter.service

# Enable and start NginX Exporter service
sudo systemctl enable nginx-exporter
sudo systemctl start nginx-exporter

# Installation cleanup
sudo rm nginx-prometheus-exporter_${NGINX_VERSION}_linux_amd64.tar.gz

# ---------------------------------------------- #
#                Install Telegraf                #
# ---------------------------------------------- #

# Configure access to NginX logs
sudo chmod 755 /var/log/nginx/access.log /var/log/nginx/error.log

# Download Telegraf
cd /tmp
wget -q https://repos.influxdata.com/influxdata-archive_compat.key
sudo echo '393e8779c89ac8d958f81f942f9ad7fb82a25e133faddaf92e15b16e6ac9ce4c influxdata-archive_compat.key' | sha256sum -c && cat influxdata-archive_compat.key | gpg --dearmor | sudo tee /etc/apt/trusted.gpg.d/influxdata-archive_compat.gpg > /dev/null
sudo echo 'deb [signed-by=/etc/apt/trusted.gpg.d/influxdata-archive_compat.gpg] https://repos.influxdata.com/debian stable main' | sudo tee /etc/apt/sources.list.d/influxdata.list
sudo apt-get update && sudo apt-get install telegraf

# Populate Telegraf configuration file
sudo echo '
#nginx-metrics and logs
[[inputs.nginx]]
    urls = ["http://localhost/metrics"]
    response_timeout = "5s"
[[inputs.tail]]
    name_override = "nginxlog"
    files = ["/var/log/nginx/access.log"]
    from_beginning = true
    pipe = false
    data_format = "grok"
    grok_patterns = ["%%{COMBINED_LOG_FORMAT}"]
' > /etc/telegraf/telegraf.conf

# Configure Telegraf for Prometheus
sudo echo '
#Output plugin
[[outputs.prometheus_client]]
    listen = "0.0.0.0:9125"
' >> /etc/telegraf/telegraf.conf

sudo systemctl restart telegraf
